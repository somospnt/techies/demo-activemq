package com.somosnt.activemq.queue;

import com.somosnt.activemq.domain.Persona;
import javax.jms.Queue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Producer {
    
    @Autowired
    private Queue queue;
    
    @Autowired
    private JmsTemplate jmsTemplate;
    
    public void enviar(Persona persona) {
        log.info("Enviando: " + persona);
        jmsTemplate.convertAndSend(queue, persona);
    }
    
}
