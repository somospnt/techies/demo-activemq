package com.somosnt.activemq.domain;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@Data
@ToString
public class Persona {
    
    @Id
    private Long id;
    private String nombre;
    
    
    
}
