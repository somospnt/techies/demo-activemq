package com.somosnt.activemq.queue;

import com.somosnt.activemq.domain.Persona;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Consumer {

    @JmsListener(destination = "persona-queue")
    public void listener(Persona persona) throws InterruptedException{
        log.info("Recibiendo: " + persona);
        Thread.sleep(10);
    }
}