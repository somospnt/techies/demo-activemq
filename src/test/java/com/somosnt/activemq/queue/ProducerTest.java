package com.somosnt.activemq.queue;

import com.somosnt.activemq.domain.Persona;
import com.somosnt.activemq.repository.PersonaRepository;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProducerTest {

    @Autowired
    private Producer producer;

    @Autowired
    private PersonaRepository personaRepository;

    @Test
    public void enviar_persona_enviaPersona() throws InterruptedException {
        Iterable<Persona> personas = personaRepository.findAll();

        personas.forEach(persona -> producer.enviar(persona));

        Thread.sleep(1000L);
    }

    @Test
    public void prueba() {
        List<Long> ids = personaRepository.buscarIds();
        ids.forEach(System.out::println);
    }

}
