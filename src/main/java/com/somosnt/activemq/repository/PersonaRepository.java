package com.somosnt.activemq.repository;

import com.somosnt.activemq.domain.Persona;
import java.util.List;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long>{
    @Query("select id from persona")
    public List<Long> buscarIds();
}
